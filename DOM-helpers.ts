import { ComponentFixture } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

/**
 * Returns the debug element for a given search item in the component fixture.
 * @param {ComponentFixture<T>} fixture - The component fixture.
 * @param {string} searchItem - The CSS selector of the search item.
 * @returns The debug element for the search item.
 */
export function getDebugElement<T>(fixture: ComponentFixture<T>, searchItem: string) {
  return fixture.debugElement.query(By.css(searchItem));
}

/**
 * Returns the inner HTML of a given search item in the component fixture.
 * @param {ComponentFixture<T>} fixture - The component fixture.
 * @param {string} searchItem - The CSS selector of the search item.
 * @returns The inner HTML of the search item.
 */
export function getInnerHtml<T>(fixture: ComponentFixture<T>, searchItem: string) {
  return fixture.debugElement.query(By.css(searchItem)).nativeElement.innerHTML;
}