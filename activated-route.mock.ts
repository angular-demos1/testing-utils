import { ActivatedRoute, convertToParamMap, Params } from '@angular/router';
import { BehaviorSubject, of } from 'rxjs';
import { Provider } from '@angular/core';

/**
 * Provides an array of providers for the ActivatedRoute.
 * @param initialRouteParams - The initial parameters for the route.
 * @returns An array of providers for the ActivatedRoute.
 */
export function provideActivatedRoute(initialRouteParams: TestRouteParams): Provider[] {
  const activatedRouteMock = new ActivatedRouteMock(initialRouteParams);

  return [
    {
      provide: ActivatedRouteMock,
      useValue: activatedRouteMock,
    },
    {
      provide: ActivatedRoute,
      useExisting: ActivatedRouteMock,
    },
  ];
}

/**
 * Interface for the parameters of a test route.
 */
export interface TestRouteParams {
  params: Params;
  queryParams: Params;
  fragment: string | null;
}

/**
 * Mock class for ActivatedRoute.
 */
export class ActivatedRouteMock {
  private innerTestParams?: Params;
  private innerTestQueryParams?: Params;
  private innerFragment?: string | null;
  private paramsSubject? = new BehaviorSubject(this.testParams);
  public params = this.paramsSubject!.asObservable();
  private queryParamsSubject? = new BehaviorSubject(this.testQueryParams);
  public queryParams = this.queryParamsSubject!.asObservable();
  private fragmentSubject? = new BehaviorSubject(this.testFragment);
  public fragment = this.fragmentSubject!.asObservable();

  /**
   * Constructor for ActivatedRouteMock.
   * @param initialRouteParams - The initial parameters for the route.
   */
  constructor(initialRouteParams?: TestRouteParams) {
    const { params, queryParams, fragment } = initialRouteParams || {
      params: {},
      queryParams: {},
      fragment: null,
    };

    this.setTestParams({ params, queryParams, fragment });
  }

  /**
   * Getter for the paramMap.
   * @returns An Observable of the paramMap.
   */
  public get paramMap() {
    return of(convertToParamMap(this.testParams));
  }

  /**
   * Getter for the snapshot.
   * @returns An object representing the snapshot.
   */
  public get snapshot(): {} {
    return {
      params: this.testParams,
      queryParams: this.testQueryParams,
      paramMap: convertToParamMap(this.testParams),
      queryParamMap: convertToParamMap(this.testQueryParams),
      root: {
        firstChild: this.firstChild,
      },
      url: null,
      fragment: this.testFragment,
    };
  }

  /**
   * Getter for the firstChild.
   * @returns An object representing the firstChild.
   */
  public get firstChild(): {} {
    return {
      snapshot: {
        paramMap: convertToParamMap(this.testParams),
        queryParamMap: convertToParamMap(this.testQueryParams),
        fragment: this.testFragment,
      },
    };
  }

  private get testParams(): Params {
    return this.innerTestParams as Params;
  }

  private get testQueryParams(): Params {
    return this.innerTestQueryParams as Params;
  }

  private get testFragment(): string | null | undefined {
    return this.innerFragment;
  }

  /**
   * Sets the test parameters.
   * @param params - The parameters for the route.
   * @param queryParams - The query parameters for the route.
   * @param fragment - The fragment for the route.
   */
  public setTestParams({ params, queryParams, fragment }: TestRouteParams) {
    this.innerTestParams = params;
    this.innerTestQueryParams = queryParams;
    this.innerFragment = fragment;

    if (this.paramsSubject) {
      this.paramsSubject.next(params);
    }

    if (this.queryParamsSubject) {
      this.queryParamsSubject.next(queryParams);
    }

    if (this.fragmentSubject) {
      this.fragmentSubject.next(fragment);
    }
  }
}
