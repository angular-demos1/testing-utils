import { Provider } from '@angular/core';

/**
 * This function creates a mock of a given class. It mocks all methods and properties of the class.
 * The mocked methods are replaced with empty functions, while the properties are replaced with getters that return an empty string.
 *
 * @template T The type of the class to be mocked.
 * @param {new (...args: any[]) => T} obj The constructor of the class to be mocked.
 * @returns {any} The mocked class.
 */
function mockClass<T>(obj: new (...args: any[]) => T): any {
  const keys = Object.getOwnPropertyNames(obj.prototype);
  const allMethods = keys.filter((key) => {
    try {
      return typeof obj.prototype[key] === 'function';
    } catch (error) {
      return false;
    }
  });
  const allProperties = keys.filter((x) => !allMethods.includes(x));

  const mockedClass = class T {};

  allMethods.forEach(
    // @ts-ignore
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    (method) => (mockedClass.prototype[method] = (): void => {})
  );

  allProperties.forEach((method) => {
    Object.defineProperty(mockedClass.prototype, method, {
      get() {
        return '';
      },
      configurable: true,
    });
  });

  return mockedClass;
}

/**
 * This function creates a mock of a given class or object. It mocks all methods and properties of the class or object.
 * The mocked methods are replaced with Jest mock functions, while the properties are replaced with getters that return an empty string.
 *
 * @template T The type of the object to be mocked.
 * @param {new (...args: any[]) => T} obj The constructor of the class to be mocked.
 * @returns {T} The mocked object.
 */
export function autoMock<T>(obj: new (...args: any[]) => T): T {
  const res = {} as any;

  const keys = Object.getOwnPropertyNames(obj.prototype);

  const allMethods = keys.filter((key) => {
    try {
      return typeof obj.prototype[key] === 'function';
    } catch (error) {
      return false;
    }
  });

  const allProperties = keys.filter((x) => !allMethods.includes(x));

  allMethods.forEach((method) => (res[method] = jest.fn()));

  allProperties.forEach((property) => {
    Object.defineProperty(res, property, {
      get: function () {
        return '';
      },
      configurable: true,
    });
  });

  return res;
}

/**
 * Provides mock for a given service
 * @param type The service type to mock
 *
 * @usage - in the provider array of the TestBed, use provideMock(MyService)
 */

export function provideMock<T>(type: new (...args: any[]) => T): Provider {
  const mock = autoMock(type);

  return { provide: type, useValue: mock };
}

/**
 * Provides a mock for a given class
 * @param type The class to mock
 *
 * @usage - in the providers array of the TestBed, use provideClassMock(MyClass)
 */

export function provideClassMock<T>(type: new (...args: any[]) => T): Provider {
  const mock = mockClass(type);

  return { provide: type, useClass: mock };
}
